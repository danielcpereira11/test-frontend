
## Candidate notes:

### Build Instructions


-> Run `npm install` on the root folder
-> If you have [Parcel](https://parceljs.org/) installed globally, just run `parcel index.html` on the root folder;
-> If you don't have it, run `npm install` and then `npm run dev` on the root folder;

### Add-ons/Packages I Used

-   Bootstrap - to speed up the layout development time;
-   Include-media - to handle Media Queries;

### Bugs/Missing Features

-   Timers;

-   There's a bug on the highlighting of the diagonal row on some use cases;

-   Some visual bugs with the resolutions in between mobile and desktop;

### Final Notes

Unfortunately, I lacked the time to go over these details/bugs and to build the timers. However, I'm proud of the end result and know I could fix them if I had the time.
Hope to hear feedback from you guys soon,
Daniel Pereira 👨‍💻
