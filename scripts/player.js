export default class Player {
	constructor(symbol, playerNumber) {
		this.symbol = symbol;
		this.score = 0;
		this.playerNumber = playerNumber;
	}

	getImage(win) {
		if (this.symbol == "x") {
			return win
				? require("../assets/images/X_bright.svg")
				: require("../assets/images/X_dark.svg");
		} else if (this.symbol == "o") {
			return win
				? require("../assets/images/O_bright.svg")
				: require("../assets/images/O_dark.svg");
		}
	}

	getName() {
		return `Player ${this.playerNumber}`;
	}

	getScoreId() {
		return this.playerNumber === 1 ? "player-1" : "player-2";
	}
}
