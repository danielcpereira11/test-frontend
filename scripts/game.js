import Player from "./player";

export default class Game {
	constructor(size) {
		this.size = size;
		this.player1 = new Player("x", 1);
		this.player2 = new Player("o", 2);
		this.currentPlayer = this.player1;
		this.score = new Array(9).fill("");
		this.round = 1;
		this.roundsWithWins = 0;
		this.board = this.getBoardMatrix();
		this.showOverlay = false;

		this.buildBoard();
	}

	buildBoard() {
		let table = document.getElementById("board");

		this.board.forEach((row, rowIndex) => {
			let tableRow = document.createElement("tr");
			row.forEach((cell, cellIndex) => {
				let tableCell = document.createElement("td");
				tableCell.setAttribute("row", rowIndex);
				tableCell.setAttribute("cell", cellIndex);
				tableCell.style.width = table.clientWidth / this.size + "px";
				tableCell.style.height = table.clientWidth / this.size + "px";

				tableCell.addEventListener("click", this.makeMove.bind(this));
				tableRow.append(tableCell);
			});
			table.append(tableRow);
		});
	}

	makeMove(e) {
		if (e.target.src) return; // Quick Hack to check if you're clicking an image and not the TD

		let row = e.target.getAttribute("row");
		let cell = e.target.getAttribute("cell");

		this.board[row][cell] = this.currentPlayer.symbol;

		e.target.innerHTML = `<img src="${this.currentPlayer.getImage(
			false
		)}">`;

		this.determineWin();
	}

	determineWin() {
		let winningRow = this.getWinningRow();

		if (winningRow) {
			this.endRound(true, winningRow);
		} else if (this.board.every(row => row.every(cell => cell !== ""))) {
			this.endRound(false);
		} else {
			this.currentPlayer =
				this.currentPlayer === this.player1
					? this.player2
					: this.player1;
		}
	}

	endRound(win, winningRow) {
		if (win) {
			this.currentPlayer.score++;
			this.highlightRow(winningRow);
			this.roundsWithWins++;
			this.score[this.round - 1] =
				this.currentPlayer == this.player1 ? "P1" : "P2";

			if (this.currentPlayer.score === 5) {
				this.endGame();
			} else {
				document.getElementById("declare-winner").innerHTML = `
            <img src="https://media.giphy.com/media/y8Mz1yj13s3kI/giphy.gif">
            <p><strong>${this.currentPlayer.getName()}</strong> wins <strong class="round">Round ${
					this.round
				}</strong></p>`;
				this.toggleOverlay();
			}
		} else {
			this.score[this.round - 1] = "T";
			document.getElementById("declare-winner").innerHTML = `
            <img src="https://media1.tenor.com/images/0ceacca8027c312fa938ace4a415d408/tenor.gif">
            <p>No one <strong>wins Round ${this.round}</strong></p>
            `;
			this.toggleOverlay();
		}

		this.updateScores();
	}

	endGame() {
		document.getElementById("declare-winner").innerHTML = `
            <img src="https://media1.tenor.com/images/5c8a343076a01d687af8a075078c5ece/tenor.gif?itemid=4950353">
            <p><strong>${this.currentPlayer.getName()}</strong> wins!</p>`;

		let resultsButton = document.getElementById("results-button");
		resultsButton.innerHTML = "Reset Game";
		resultsButton.setAttribute("onClick", "window.game.resetGame()");

		this.toggleOverlay();

		setTimeout(() => {
			document.getElementById("statistics").scrollIntoView({
				behavior: "smooth"
			});
		}, 1000);
	}

	toggleOverlay() {
		if (this.showOverlay) {
			document.getElementById("score-overlay").style.display = "none";
			this.showOverlay = false;
		} else {
			document.getElementById("score-overlay").style.display = "flex";
			this.showOverlay = true;
		}
	}

	getWinningRow() {
		let horizontalWinRow = this.checkHorizontal();

		if (horizontalWinRow) return horizontalWinRow;

		let verticalWinRow = this.checkColumns();

		if (verticalWinRow) return verticalWinRow;

		let diagonalWinRow = this.checkDiagonal();

		if (diagonalWinRow) return diagonalWinRow;

		return null;
	}

	highlightRow(winningRow) {
		let board = document.getElementById("board");
		let cells = Object.values(board.getElementsByTagName("td"));

		let winningCells = winningRow.map(
			row =>
				cells.filter(
					cell =>
						cell.getAttribute("row") == row[0] &&
						cell.getAttribute("cell") == row[1]
				)[0]
		);

		winningCells.forEach(cell => {
			cell.innerHTML = `<img src="${this.currentPlayer.getImage(true)}">`;
			// cell.style.backgroundColor = "green";
		});
	}

	checkHorizontal() {
		let winningRow = this.board.findIndex(row =>
			row.every(cell => cell === this.currentPlayer.symbol)
		);

		if (winningRow >= 0) {
			let indexes = [];

			this.board[winningRow].forEach((cell, index) => {
				indexes.push([winningRow, index]);
			});

			return indexes;
		}

		return null;
	}

	checkColumns() {
		let indexes = [];

		for (let i = 0; i < this.size; i++) {
			for (let x = 0; x < this.size; x++) {
				if (this.board[x][i] === this.currentPlayer.symbol) {
					indexes.push([x, i]);
				}
			}
			if (indexes.length == this.size) {
				return indexes;
			} else {
				indexes = [];
			}
		}

		//return count == this.size;
	}

	checkDiagonal() {
		let diagonalRowLeft = this.board.filter(
			(row, index) => row[index] === this.currentPlayer.symbol
		);

		if (diagonalRowLeft.length == this.size) {
			return diagonalRowLeft.map((row, index) => {
				return [
					index,
					row.findIndex(cell => cell == this.currentPlayer.symbol)
				];
			});
		} else {
			let diagonalRowRight = [...this.board]
				.reverse()
				.filter(
					(row, index) => row[index] === this.currentPlayer.symbol
				);

			if (diagonalRowRight.length === this.size) {
				return diagonalRowRight.reverse().map((row, index) => {
					return [
						index,
						row.findIndex(cell => cell == this.currentPlayer.symbol)
					];
				});
			}
		}

		return null;
	}

	getBoardMatrix() {
		let board = Array(this.size);

		// Build Columns
		for (var i = 0; i < this.size; i++) {
			board[i] = new Array(this.size).fill("");
		}

		return board;
	}

	resetBoard() {
		let board = document.getElementById("board");
		board.innerHTML = "";
	}

	resetGame(size) {
		if (size) this.size = parseInt(size);
		if (this.showOverlay) this.toggleOverlay();
		this.player1.score = 0;
		this.player2.score = 0;
		this.round = 1;

		// Reset Overlay Button
		let resultsButton = document.getElementById("results-button");
		resultsButton.setAttribute("onClick", "window.game.nextRound()");
		resultsButton.innerHTML = "Next Round";

		this.updateScores();
		this.resetScores();
		this.resetBoard();
		this.board = this.getBoardMatrix();
		this.buildBoard();
		this.currentPlayer = this.player1;
	}

	nextRound() {
		this.resetBoard();
		this.buildBoard();
		this.round++;
		this.board = this.getBoardMatrix();
		this.toggleOverlay();
		this.currentPlayer = this.player1;
	}

	resetScores() {
		// Reset Board Score Elements
		document.getElementById(
			this.player1.getScoreId() + "-score"
		).innerHTML = 0;

		document.getElementById(
			this.player2.getScoreId() + "-score"
		).innerHTML = 0;

		// Reset Percentages
		document.getElementById(
			this.player1.getScoreId() + "-win-percentage"
		).innerHTML = "";
		document.getElementById(
			this.player1.getScoreId() + "-loss-percentage"
		).innerHTML = "";

		document.getElementById(
			this.player2.getScoreId() + "-win-percentage"
		).innerHTML = "";
		document.getElementById(
			this.player2.getScoreId() + "-loss-percentage"
		).innerHTML = "";

		// Reset Matches Log
		Object.values(
			document.getElementById("matches-log").getElementsByTagName("li")
		).forEach(dot => dot.classList.remove("played"));

		// Reset Game History
		Object.values(
			document.getElementById("game-history").getElementsByTagName("li")
		).forEach(square => (square.innerHTML = ""));
	}

	updateScores() {
		// Handle Board Score
		let gameBoardScore = document.getElementById(
			this.currentPlayer.getScoreId() + "-score"
		);

		gameBoardScore.innerHTML = this.currentPlayer.score;

		// Handle Statistics - Percentages
		let player1WinPercentage =
			Math.round((this.player1.score / this.roundsWithWins) * 100) + "%";
		let player2WinPercentage =
			Math.round((this.player2.score / this.roundsWithWins) * 100) + "%";

		document.getElementById(
			this.player1.getScoreId() + "-win-percentage"
		).innerHTML = player1WinPercentage;
		document.getElementById(
			this.player1.getScoreId() + "-loss-percentage"
		).innerHTML = player2WinPercentage;

		document.getElementById(
			this.player2.getScoreId() + "-win-percentage"
		).innerHTML = player2WinPercentage;
		document.getElementById(
			this.player2.getScoreId() + "-loss-percentage"
		).innerHTML = player1WinPercentage;

		// Handle Matches Played Log
		Object.values(
			document.getElementById("matches-log").getElementsByTagName("li")
		)[this.round - 1].classList.add("played");

		// Handle Game History
		Object.values(
			document.getElementById("game-history").getElementsByTagName("li")
		)[this.round - 1].innerHTML = `<span class="win">${
			this.score[this.round - 1]
		}</span>`;
	}

	handleResize() {
		let table = document.getElementById("board");

		Object.values(table.getElementsByTagName("td")).forEach(cell => {
			cell.style.width = table.clientWidth / this.size + "px";
			cell.style.height = table.clientWidth / this.size + "px";
		});
	}
}
