import "./assets/styles/main.scss";

import Game from "./scripts/game";

function startGame() {
	let game = new Game(3);
	window.game = game;
}

window.onload = startGame;
window.onresize = function() {
	window.game.handleResize();
};
